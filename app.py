from flask.ext.sqlalchemy import SQLAlchemy
from flask import Flask, render_template, request
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import json


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:tanshu@localhost/webScrapper' #ADDRESS OF DATABASE
db = SQLAlchemy(app)
driver = webdriver.PhantomJS()

unprocessed_addresses=[]
realEstateUnprocessedUrl =[]
domainUnprocessedUrl =[]
onTheHouseUnprocessedUrl =[]
walkScoreUrl = []

class Data(db.Model):
    __tablename__ = "initialData"
    id = db.Column(db.Integer, primary_key=True)
    realEstateUrl_ = db.Column(db.String(2400), unique = False)
    domainUrl_ = db.Column(db.String(2400), unique = False)
    onTheHouseUrl_ = db.Column(db.String(2400), unique = False)
    address_ = db.Column(db.String(240), unique = False)
    suburb_ = db.Column(db.String(50), unique = False)
    state_ = db.Column(db.String(33), unique = False)
    pincode_ = db.Column(db.Integer, unique = False)
    processed_ = db.Column(db.Boolean, unique=False, default = False)

    def __init__(self, realEstateUrl_, domainUrl_, onTheHouseUrl_,address_, suburb_, state_, pincode_, processed_):
        self.realEstateUrl_ = realEstateUrl_
        self.domainUrl_ = domainUrl_
        self.onTheHouseUrl_ =onTheHouseUrl_
        self.address_ = address_
        self.suburb_ = suburb_
        self.state_ = state_
        self.pincode_ = pincode_
        self.processed_ = processed_

class info(db.Model):
    __tablename__ = "info"
    propertyId_ = db.Column(db.Integer,primary_key=True,unique=False)
    provider_ = db.Column(db.String(50),primary_key=True,unique=False)
    data_ = db.Column(db.String(2048),unique=False)
    # __table_args__ = (
    #     UniqueConstraint("propertyId_)", "provider_"),
    # )
    def __init__(self, propertyId_,provider_,data_ ):
        self.propertyId_ = propertyId_
        self.provider_ = provider_
        self.data_ = data_

def realEstate(addressInfo):
    d={}
    try:
        r=requests.get(addressInfo)
        c=r.content
    except:
        pass
    try:
        soup=BeautifulSoup(c,"html.parser")
        address=soup.find_all("h3",{"class":"address"})
        d["Address"] = address[0].text
    except:
        d["Address"] = "Not mentioned"
    try:
        features = soup.find_all("div", {"id":"listing_info"})
        features = features[0]
    except:
        pass
    try:
        d["bedroom"] = features[0].text
    except:
        d["bedroom"] = "Not mentioned"
    try:
        d["bathrooms"] = features[1].text
    except:
        d["bathrooms"] = "Not mentioned"
    try:
        d["garage"] = features[2].text
    except:
        d["garage"] = "Not mentioned"
    try:
        info = soup.find_all("div", {"id":"listing_info"})
        info = info[0]("p")
    except:
        pass
    try:
        d["Status"] = info[0].text
    except:
        d["Status"] = "Not mentioned"

    try:
        titleDescription = soup.find_all("div", {"id":"description"})
        titleDescription = titleDescription[0]({"p"}, {"class","title"})
    except:
        pass
    try:
        d["Title"] = titleDescription[0].text
    except:
        d["Title"] = "Not mentioned"
    try:
        fullDescription = soup.find_all("div", {"id":"description"})
        fullDescription = fullDescription[0]({"p"}, {"class","body"})
    except:
        pass
    try:
        d["Description"] = fullDescription[0].text
    except:
        d["Description"] = "Not mentioned"
    try:
        fullFeatures = soup.find_all("div",{"id":"features"})
        fullFeatures = fullFeatures[0].find_all("div", {"class","featureList"})
        fullFeatures = fullFeatures[0].find_all("ul")
        overall =""
        for i in range(0,len(fullFeatures)):
            overall += fullFeatures[i].text
            overall = overall + " "
    except:
        pass
    try:
        d["Features"] = overall
    except:
        d["Features"] = "Not mentioned"

    try:
        d["Image Url"] = images['src']
    except:
        d["Image Url"] = "Not mentioned"

    return d


def domain(address):
    d={}
    try:
        r=requests.get(address)
        c=r.content
    except:
        pass
    try:
        soup=BeautifulSoup(c,"html.parser")
        address=soup.find_all("div",{"class":"left-wrap"})
        address=soup.find("h1")

        d["Address"] = address.text.replace("\r","").replace("\n","").replace("  ","")
    except:
        d["Address"] = "Not mentioned"
    try:
        price = soup.find_all("span",{"class":"h4 pricedisplay"})
        d["Price"] = price[0].text

        features = soup.find_all("div", {"class":"listing-features alt"})
        features = features[0]("span", {"class":"f-icon with-text"})
    except:
        pass
    try:
        d["bedroom"] = features[0].text
    except:
        d["bedroom"] = "Not mentioned"
    try:
        d["bathrooms"] = features[1].text
    except:
        d["bathrooms"] = "Not mentioned"
    try:
        d["garage"] = features[2].text
    except:
        d["garage"] = "Not mentioned"
    try:
        titleDescription = soup.find_all("h4", {"class":"h5 lowlight"})
    except:
        pass
    try:
        d["Title"] = titleDescription[0].text
    except:
        d["Title"] = "Not mentioned"
    try:
        fullDescription = soup.find_all("div", {"class":"property-description truncate_textblock"})
        fullDescription = fullDescription[0]("p")
    except:
        pass
    try:
        d["Description"] = fullDescription[0].text.replace("*",",").replace("\r", "").replace("\n","").replace("  ","")
    except:
        d["Description"] = "Not mentioned"

    return d

def onTheHouse(address):
    d={}
    try:
        driver.get(address)
    # for address
    except:
        pass
    try:
     d["Address"] = driver.find_element_by_class_name('address').text
    except:
     d["Address"] = "Not Mentioned"

    #for features
    try:
        features = driver.find_elements_by_class_name('property-attribute')
    except:
        pass
    try:
        d["Bedroom"] = features[0].text
    except:
        d["Bedroom"]= "Not mentioned"

    try:
        d["Bathrooms"] = features[1].text
    except:
        d["Bathrooms"] = "Not mentioned"

    try:
        d["Parking"] = features[2].text
    except:
        d["Parking"] = "Not mentioned"

    #calculate estimates, low & high, accuracy
    try:
        estimatedMain = driver.find_element_by_class_name("estimate-main-price")
    except:
        pass

    try:
        d["Calculated Estimate"] = estimatedMain.text
    except:
        d["Calculated Estimate"] = "Not Mentioned"

    try:
        estimatedLowHigh = driver.find_elements_by_class_name("estimate-price")
    except:
        pass

    try:
        d["Lower Estimate"] = estimatedLowHigh[0].text
    except:
        d["Lower Estimate"] = "Not Mentioned"

    try:
        d["Higher Estimate"] = estimatedLowHigh[1].text
    except:
        d["Higher Estimate"] = "Not Mentioned"

    try:
        AccuracyLevel = driver.find_element_by_class_name("accuracy-level")
    except:
        pass
    try:
        d["Accuracy Level"] = AccuracyLevel.text
    except:
        d["Accuracy Level"] = "Not Mentioned"

    try:
        propertyInfo = driver.find_elements_by_class_name("legal-attribute-row")
        propertyInfoKey = []
        propertyInfoValue = []
        for i in range(0, len(propertyInfo)):
            key = propertyInfo[i].find_elements_by_class_name("name")
            value = propertyInfo[i].find_elements_by_class_name("value")
            propertyInfoKey.append(key[0].text)
            propertyInfoValue.append(value[0].text)

        for i in range(0, len(propertyInfoKey)):
            if(propertyInfoKey[i] != ''):
                 d[propertyInfoKey[i]] = propertyInfoValue[i]
        print(d)
        driver.quit()
        return d
    except:
        return d

def walkScore():
    pass
# data = [{
#   "realEstateUrl": "bootstrap-table",
#   # "commits": "10",
#   # "attention": "122",
#   # "uneven": "An extended Bootstrap table"
# },
#  {
#   "realEstateUrl": "multiple-select",
#   # "commits": "288",
#   # "attention": "20",
#   # "uneven": "A jQuery plugin"
#  }]
# # , {
# #   "name": "Testing",
# #   "commits": "340",
# #   "attention": "20",
# #   "uneven": "For test"
# # }]

@app.route("/")
def index():
    data = retrieve()
    print(data)
    return render_template("index.html", data = json.loads(data))



# Retrieve of Property Details API, this will return the details of given property
@app.route("/property/<id>", methods=['GET'])
def retrieve_property(id):
    returnValue = []
    for d in db.session.query(info).filter(info.propertyId_ == id).all():
        doc = {}
        doc["id" ] = (d.propertyId_)
        doc["Provider"]=d.provider_
        doc["Data" ]=d.data_
        returnValue.append(doc)
        print("asdsadadasdasfwagfewa")
    return json.dumps(returnValue)

# Retrieve of Property API, this will return all the properties
@app.route("/property", methods=['GET'])
def retrieve():
    # select * from initialData and return
    returnValue = []
    for d in db.session.query(Data).all():
        doc = {}
        doc["id" ] = d.id
        doc["realEstateUrl" ] = d.realEstateUrl_
        doc["domainUrl"] = d.domainUrl_
        doc["onTheHouseUrl"] = d.onTheHouseUrl_
        doc["address"] = d.address_
        doc["suburb"] = d.suburb_
        doc["state"] = d.state_
        doc["pincode"] = d.pincode_
        returnValue.append(doc)
        # print("asdasd")
        # print(json.dumps(returnValue))
    return json.dumps(returnValue)


# @app.route("/property/<id>", methods = ['PUT'])
# def update_property(id):
#     incoming_property = request.json
#     print("asd")
#     print(incoming_property)
#     #update the given property whose id is id and data is new data
#     bug = "as"
#     domainUrl = ""
#     onTheHouseUrl = ""
#     address = ""
#     suburb = ""
#     state = ""
#     pincode = ""
#
#     if realEstateUrl is None:
#         pass
#     else:
#         for d in db.session.query(Data).filter(Data.id == id).all():
#             # d.realEstateUrl_ = realEstateUrl
#             print(realEstateUrl)
#             d.update(d.realEstateUrl_ = bug)
#             print(type((d.realEstateUrl_)))
#             db.session.commit()
#     for d in db.session.query(Data).all():
#         print(d.realEstateUrl_)
#
#     return "as"




# this becomes CREATE of property and it returns the property with id
@app.route("/success", methods = ['POST'])
def success():
    if request.method == 'POST':
        realEstateUrl_ = request.form["REURL"]
        domainUrl_ = request.form["DomainURL"]
        onTheHouseUrl_ = request.form["OnTheHouseURL"]
        address_ = request.form["Address"]
        suburb_ = request.form["Suburb"]
        state_ = request.form["State"]
        pincode_ = request.form["Pincode"]
        processed_ = 'False'
        print(request.form)
        data = Data(realEstateUrl_, domainUrl_,onTheHouseUrl_,address_,suburb_,state_,pincode_,processed_)
        db.session.add(data)
        db.session.commit()
        getInfo()
        return render_template("success.html")

def getInfo():
    for d in db.session.query(Data).filter(Data.processed_ == 'False').all():
        print("a")

        returnValueRE = json.dumps(realEstate(d.realEstateUrl_))
        dataRE = info(d.id,"Real Estate", returnValueRE)
        # print(dataRE)
        # print(returnValueRE)
        db.session.add(dataRE)
        # db.session.commit()

        # returnValueDomain = json.dumps(domain(d.domainUrl_))
        returnValueDomain =  json.dumps(domain(d.domainUrl_))
        # print(returnValueDomain)
        dataDomain = info(d.id,"Domain", returnValueDomain)
        db.session.add(dataDomain)

        returnValueOTH = json.dumps(onTheHouse(d.onTheHouseUrl_))
        dataHouse = info(d.id,"OnTheHouse", returnValueOTH)
        db.session.add(dataHouse)
        db.session.commit()


        d.processed_ = 'True'
        db.session.commit()




if __name__ == '__main__':
    app.debug=True
    # addMore()
    print("in main")
    app.run()
