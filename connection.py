from flask.ext.sqlalchemy import SQLAlchemy
from flask import Flask, render_template, request
import requests
from bs4 import BeautifulSoup
from realEstate import realEstate

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:tanshu@localhost/webScrapper' #ADDRESS OF DATABASE
db = SQLAlchemy(app)
