import connection.py
class realEstate(db.Model):
    __tablename__ = "realEstate"
    id = db.Column(db.Integer, primary_key=True)
    data_ = db.Column(db.String(1024), unique = True)

    def __init__(self, data_):
        self.data_= data_

    def realEstate(addressInfo):
        r=requests.get(addressInfo[4])
        c=r.content
        d={}
        soup=BeautifulSoup(c,"html.parser")
        address=soup.find_all("h3",{"class":"address"})
        d["Address"] = address[0].text
        features = soup.find_all("div", {"id":"listing_info"})
        features = features[0]("dd")
        try:
            d["bedroom"] = features[0].text
        except:
            d["bedroom"] = "Not mentioned"
        try:
            d["bathrooms"] = features[1].text
        except:
            d["bathrooms"] = "Not mentioned"
        try:
            d["garage"] = features[2].text
        except:
            d["garage"] = "Not mentioned"

        info = soup.find_all("div", {"id":"listing_info"})
        info = info[0]("p")
        try:
            d["Status"] = info[0].text
        except:
            d["Status"] = "Not mentioned"

        titleDescription = soup.find_all("div", {"id":"description"})
        titleDescription = titleDescription[0]({"p"}, {"class","title"})

        try:
            d["Title"] = titleDescription[0].text
        except:
            d["Title"] = "Not mentioned"

        fullDescription = soup.find_all("div", {"id":"description"})
        fullDescription = fullDescription[0]({"p"}, {"class","body"})

        try:
            d["Description"] = fullDescription[0].text
        except:
            d["Description"] = "Not mentioned"

        fullFeatures = soup.find_all("div",{"id":"features"})
        fullFeatures = fullFeatures[0].find_all("div", {"class","featureList"})
        fullFeatures = fullFeatures[0].find_all("ul")
        overall =""
        for i in range(0,len(fullFeatures)):
            overall += fullFeatures[i].text
            overall = overall + " "
        try:
            d["Features"] = overall
        except:
            d["Features"] = "Not mentioned"

            # images = soup.find_all("div", {"id":"mainPhoto"})
            # images = images[0].find(("img",{"class":"hero-image__link"}))
            # print(images)

        print(d)
